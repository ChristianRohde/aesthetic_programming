var tv;

function preload() {
tv = loadImage('oldtv1.png');
}

function setup() {
  // put setup code here
  let canvas = createCanvas(640,410);
  canvas.position(50,125);
  print("hello world");
  //You can also use console.log which is (js)
  frameRate(0.5);
}

function draw() {
  // put drawing code here
background(255);
//red ellipse
  fill(255,0,0,80);
  noStroke();
  ellipse((random(640)),(random(480)),400,400);
//green ellipse
  fill(0,255,0,80);
  noStroke();
  ellipse((random(640)),(random(480)),400,400);
//blue ellipse
  fill(0,0,255,80);
  noStroke();
  ellipse((random(640)),(random(480)),400,400);

  imageMode(CENTER);
  image(tv,320,200,650,640);
}
